import passport from 'passport';

const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

passport.use(new GoogleStrategy({
  clientID: '354232569794-uiq7e3s9vlm9029qsr7rmk14l69fvan8.apps.googleusercontent.com',
  clientSecret: '82_Q8gSLiysY0k1IuAHa12ac',
  callbackURL: 'http://localhost:3000/google/callback'
},
((accessToken, refreshToken, profile, done) => {
  return done(null, profile);
})));
