import axios from 'axios';
import TYPE from '../../types/cart';


export default function addItemToCart(item) {
  return (dispatch) => {
    dispatch({ type: TYPE.REQ_DATA });
    return axios.post('http://localhost:3000/v1.0/cart/add', { item: item }).then(response => {
      console.log(response);
      dispatch({
        type : TYPE.RES_DATA, 
        data: response.data.response.updatedCart,
      });
    });
  };
}
