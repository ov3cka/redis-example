import axios from 'axios';
import TYPE from '../../types/cart';

export default function removeItemFromCart(item) {
  return (dispatch) => {
    dispatch({ type: TYPE.REQ_REMOVE_ITEM_CART });
    return axios.get(`http://localhost:3000/v1.0/cart/remove/${item.id}`).then(response => {
      console.log(response);
      dispatch({
        type : TYPE.RES_REMOVE_ITEM_CART,
        data: response.data.response.updatedCart,
      })
    })
  };
}
