import axios from 'axios';
import TYPE from '../../types/cart';


export default function getCart() {
  return (dispatch) => {
    dispatch({ type: TYPE.REQ_GET_CART });
    return axios.get('http://localhost:3000/v1.0/cart/').then(response => {
      console.log(response);
      dispatch({
        type: TYPE.RES_GET_CART, 
        data: response.data.cart,
      });
    });
  };
}
