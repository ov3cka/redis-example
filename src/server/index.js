import Express from 'express';
import passport from 'passport';
import cookieSession from 'cookie-session';
import handleRender from './handleRender';
import { products } from './data/products';

require('../../passport-setup');

const app = Express();
const port = 3000;

app.use(cookieSession({
  name: 'redis-session',
  keys: ['profile', 'name']
}));

const isLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    res.sendStatus(401);
  }
};

app.use(passport.initialize());
app.use(passport.session());

// app.get('/login', (req, res) => res.send('You are not loged in'));

app.get('/google',
  passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/google/callback',
  passport.authenticate('google', { failureRedirect: '/failed' }),
  (req, res) => {
    res.redirect('/good');
  });

app.get('/failed', (req, res) => res.send('You failed to Log in'));
app.get('/good', isLoggedIn, (req, res) => res.send(`Welcome ${req.user.displayName}!`));
app.get('/logout', (req, res) => {
  req.session = null;
  req.logout();
  res.redirect('/');
});

const cookie = require('cookie');
const cookieParser = require('cookie-parser');
const uuidv1 = require('uuid/v1');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use((req, res, next) => {
  if (!req.cookies.sessionId) {
    res.setHeader('Set-Cookie', cookie.serialize('sessionId', uuidv1(), {
      httpOnly: true,
      maxAge: 60 * 60 * 24 * 7
    }));
  }
  next();
});

require('./api/routes/main')(app);

app.get('/v1.0/products', (req, res) => {
  res.json({ products }).status(200);
});

// server static content
app.use('/dist', Express.static('dist'));

// register route handler
app.use(handleRender);

// listen out for incoming requests
app.listen(port, () => {
  console.log('app now listening on port', port);
});
