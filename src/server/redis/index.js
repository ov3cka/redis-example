const redis = require('redis');

const client = redis.createClient({
  port: '12191',
  host: 'redis-12191.c135.eu-central-1-1.ec2.cloud.redislabs.com',
});

client.auth('GbyQwja8xrbUu02XA09nlWqTZzVPfaC0', (error, reply) => {
  if (!error) {
    console.log(reply);
  }
});

const addToCart = (sessionId, item) => {
  return new Promise((resolve, reject) => {
    getCart(sessionId).then(cart => {
      if (cart === null) {
        const newCart = [item];
        client.set(sessionId, JSON.stringify(newCart), (err, stored) => {
          if (stored) {
            getCart(sessionId).then(cart => { resolve({ stored: true, value: stored, updatedCart: JSON.parse(cart) })});
          }
        })
      }
      else {
        const parsedCart = JSON.parse(cart);
        parsedCart.push(item);
        client.set(sessionId, JSON.stringify(parsedCart), (error, stored) => {
          if (stored) {
            getCart(sessionId).then(cart => { resolve({ stored: true, value: stored, updatedCart: JSON.parse(cart) })});
          }
        })
      }
    })
  });
};

const removeFromCart = (sessionId, id) => {
  return new Promise((resolve, reject) => {
    getCart(sessionId).then(cart => {
      const parsedCart = JSON.parse(cart);
      const newCart = parsedCart.filter(item => item.id !== id);
      client.set(sessionId, JSON.stringify(newCart), (error, stored) => {
        if (stored) {
          getCart(sessionId).then(cart => { resolve({ stored: true, value: stored, updatedCart: JSON.parse(cart) })});
        };
      });
    });
  });
};

const getCart = (sessionId) => {
  return new Promise((resolve, reject) => {
    client.get(sessionId, (error, cart) => {
      if (!error) {
        resolve(cart);
      }
    });
  });
};

// client.set('a', 'one', (error, reply) => {
//   if (!error) {
//     getCart('a').then((cart) => { console.log( cart ) });
//   }
// });

// addToCart('j', {id: 5, name: 'ham', weight: '250g'}).then((result) => {
//   console.log(result);
//   getCart('j').then( cart => {
//     console.log(cart);
//   });
// });

// removeFromCart('j', 5).then(res => {
//   console.log(res);
// });

module.exports = { getCart, addToCart, removeFromCart }
