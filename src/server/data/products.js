/* eslint-disable import/prefer-default-export */
export const products = [
  {
    id: '1',
    name: 'Rustic Steel Cheese',
    image: 'http://lorempixel.com/640/480/transport',
    price: '940.00',
    description: 'hierarchy'
  },
  {
    id: '2',
    name: 'Sleek Fresh Soap',
    image: 'http://lorempixel.com/640/480/animals',
    price: '119.00',
    description: 'Books'
  },
  {
    id: '3',
    name: 'Licensed Steel Hat',
    image: 'http://lorempixel.com/640/480/fashion',
    price: '726.00',
    description: 'Coordinator Associate Soft'
  },
  {
    id: '4',
    name: 'Licensed Granite Salad',
    image: 'http://lorempixel.com/640/480/business',
    price: '158.00',
    description: 'bluetooth incubate'
  },
  {
    id: '5',
    name: 'Awesome Rubber Towels',
    image: 'http://lorempixel.com/640/480/technics',
    price: '878.00',
    description: 'orchestrate Seychelles'
  }
];
