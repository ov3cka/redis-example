import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Box } from '@material-ui/core';
import { Product } from '../product';

export default class Cart extends Component {
  constructor(props) {
    super();
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
  }

  render() {
    return (
      <div>
        <h1>Cart ({this.props.cart.length})</h1>
        <Box display="flex">
          {this.props.cart.map((item, key) => <Product key={`product-${key + 1}`} {...item} onBtnClick={this.props.removeItemHandler} btnText="Remove Item" />)}
        </Box>
      </div>
    );
  }
}

Cart.propTypes = {
};
