import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const Nav = ({ showCartHandler }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Button color="inherit" onClick={showCartHandler}>Basket</Button>
          <Button color="inherit"><Link to="/google" replace>Login</Link></Button>
          <Button color="inherit"><Link to="/logout" replace>Logout</Link></Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Nav;
